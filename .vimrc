"这个档案的双引号 (") 是批注
set fencs=utf-8,gbk,utf-16,utf-32,ucs-bom
"引号代表注释
set hlsearch                  "高亮度反白
set backspace=2               "可随时用倒退键删除
set autoindent                 "自动缩排
set ruler                      "可显示最后一行的状态
set showmode                 "左下角那一行的状态
set nu                        "可以在每一行的最前面显示行号
set bg=dark                   "显示不同的底色色调
syntax on                     "进行语法检验，颜色显示
set wrap                      "自动折行
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab                  "将tab替换为相应数量空格
set smartindent
"nerdtree 配置
autocmd VimEnter * NERDTree
wincmd w
autocmd VimEnter * wincmd w

filetype plugin on                                             
autocmd FileType php set omnifunc=php#CompletePHP
map <F10>:set paste<CR>

map <F11>:set nopaste<CR>
set pastetoggle=<F11>

set encoding=utf8               "设置内部编码为utf8
set fileencoding=utf8            "当前编辑的文件编码
set fileencodings=uft8-bom,utf8,gbk,gb2312,big5   "打开支持编码的文件

filetype plugin on                                             
autocmd FileType php set omnifunc=phpcomplete#CompletePHP

if &term=="xterm"
  set t_Co=8
  set t_Sb=^[[4%dm
  set t_Sf=^[[3%dm
endif

let g:neocomplcache_enable_at_startup = 1

map <F8> :Calendar<CR>		" calendar 定义时间插件快捷方式为 F8

